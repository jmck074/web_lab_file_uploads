package ictgradschool.web.lab.uploads;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FileUploads extends HttpServlet {
    private File uploadsFolder;
    private File tempFolder;

    @Override
    public void init() throws ServletException {
        super.init();
        //this next line is very important for getting the relative file paths ESSENTIAL
        this.uploadsFolder = new File(getServletContext().getRealPath("/Uploaded-Photos"));
        if (!uploadsFolder.exists()) {
            uploadsFolder.mkdirs();
        }
        this.tempFolder = new File(getServletContext().getRealPath("/WEB-INF/temp"));
        if (!tempFolder.exists()) {
            tempFolder.mkdirs();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
    // DiskFileItemFactory has APIs to extract stream
        //these are the apache classes we needed to import from the jar files and add to the library
        DiskFileItemFactory factory = new DiskFileItemFactory();
        //either setting max size, or the size they go into the factory
        factory.setSizeThreshold(4*1024);
        //factory will work in temporary folder, temp folder is part of the upload process we shouldn't need to access it after
    factory.setRepository(tempFolder);
    //ServletFileUpload is an API for processing file uploads
    ServletFileUpload upload = new ServletFileUpload(factory);
        System.out.println(upload);
    resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
    try{
        //gets everything that was submitted in the form a list of file items
        List<FileItem> fileItems = upload.parseRequest(req);
        //take a file from the form and put it in the server need to create  file on the server
        System.out.println(fileItems);
        //Basically we are taking all the files being uploaded, get the names and then create empty files on the server with the same names
        File fullsizeImageFile = null;

        for(FileItem fi: fileItems){
            if(!fi.isFormField()){
                String filename = fi.getName();
                System.out.println("anything");
                fullsizeImageFile = new File(uploadsFolder, filename);
                //basically copying the fi file to the fullsizeimage
                //or rather writing fi on fullsizeImage
                fi.write(fullsizeImageFile);
            }
        }
out.println("<img src=\"../Uploaded-Photos/"+ fullsizeImageFile.getName() +" \" width = \"200\">");

    } catch (Exception e){throw new ServletException(e);}




    }

    //TODO - make this a proper HttpServlet that uses init() and doPost() methods

}
